#define HEAP_SIZE 4096

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdlib.h>

void test1_standart_memory_allocate();

void test2_memory_free_one_block();

void test3_memory_free_two_blocks();

void test4_new_region_from_old();

void test5_new_region_from_new();
