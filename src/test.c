#include "test.h"

#define MEMORY_SIZE 100
#define SECOND_SIZE 2000

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}


//Обычное успешное выделение памяти.
void test1_standart_memory_allocate(){
    fprintf(stdout, "Starting test №1! \n");
    void* data = _malloc(MEMORY_SIZE);
    if (data == NULL){
        err("Test №1 failed (memory not allocated)");
    }
    _free(data);
    fprintf(stdout, " №1 passed\n");
    
}
//Освобождение одного блока из нескольких выделенных.
void test2_memory_free_one_block(){
    fprintf(stdout, "Starting test №2! \n");
    void *heap = heap_init(HEAP_SIZE);
    if (heap == NULL) {
        err("Test №2 failed (heap initialization failed!)\n");

    }
    void *data1 = _malloc(MEMORY_SIZE);
    void *data2 = _malloc(MEMORY_SIZE);

    if (data1 == NULL || data2 == NULL){
        err("Test №2 failed (memory not allocated!)");
    }

    struct block_header *data1_block = block_get_header(data1);
    struct block_header *data2_block = block_get_header(data2);

    _free(data1);
    if (!data1_block->is_free || data2_block->is_free){
        err("Test №2 failed (wrong order!)");
    }

    _free(data2);
    fprintf(stdout,"Test №2 passed\n");
}
//Освобождение двух блоков из нескольких выделенных.
void test3_memory_free_two_blocks(){
    fprintf(stdout, "Starting test №3! \n");
    void *heap = heap_init(HEAP_SIZE);
    if (heap == NULL) {
        err("Test №3 failed (heap initialization failed!)\n");
    }
    void* data1 = _malloc(MEMORY_SIZE);
    void* data2 = _malloc(MEMORY_SIZE);
    void* data3 = _malloc(MEMORY_SIZE);
    if (data1 == NULL || data2 == NULL || data3 == NULL){
        err("Test №3 failed (memory not allocated!)");
    }
    _free(data2);
    _free(data1);
    debug_heap(stdout, heap);
    struct block_header* data1_block = block_get_header(data1);
    struct block_header* data3_block = block_get_header(data3);
    if (data1_block -> is_free || !data3_block -> is_free){
        err("Test №3 failed (wrong order!)");
    }
    if (data1_block->capacity.bytes != SECOND_SIZE + offsetof(struct block_header, contents))
        err("Test №3 failed (incorrect size!)");
    _free(data3);
    fprintf(stdout,"Test №3 passed\n");

}
//Память закончилась, новый регион памяти расширяет старый.
void test4_new_region_from_old(){
    fprintf(stdout, "Starting test №4! \n");
    void* heap = heap_init(HEAP_SIZE);
    if (heap == NULL){
        err("Test №4 failed (heap initialization failed!)");
    }
    void* data1 = _malloc(MEMORY_SIZE);
    void* data2 = _malloc(MEMORY_SIZE);
    void* data3 = _malloc(MEMORY_SIZE);
    if (data1 == NULL || data2 == NULL || data3 == NULL){
        err("Test №4 failed (memory not allocated!)");
    }
    struct block_header* data1_block = block_get_header(data1);
    struct block_header* data2_block = block_get_header(data2);
    struct block_header* data3_block = block_get_header(data3);
    if (data1_block->next != data2_block || data2_block->next != data3_block){
        err("Test №4 failed (incorrect blocks ordering!)");
    }
    void* data4 = _malloc(MEMORY_SIZE);
    if (data4 == NULL){
        err("Test №4 failed (memory not extended!)");
    }
    struct block_header* data4_block = block_get_header(data4);
    if (data3_block->next != data4_block){
        err("Test №4 failed (incorrect blocks ordering!)");
    }
    fprintf(stdout,"Test №4 passed\n");     
}
//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов,
// новый регион выделяется в другом месте.

void test5_new_region_from_new(){

  fprintf(stdout, "Starting test №5! \n");

    void* heap = heap_init(HEAP_SIZE);
    if (heap == NULL) {
        err("Test №5 failed (heap initialization failed!)\n");
    }

    void* data1 = _malloc(MEMORY_SIZE*5);
    void* data2 = _malloc(MEMORY_SIZE*5);
    if (data1 == NULL || data2 == NULL) {
        err("Test №5 failed (memory not allocated!)");
    }

    void* data3 = _malloc(MEMORY_SIZE*5);
    if (data3 == NULL) {
        err("Test №5 failed (memory not allocated!)");
    }
    if (data3 == heap) {
        err("Test №5 failed (new region not allocated in different place!)");
    }

}
