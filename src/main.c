#include "test.h"

int main() {
    test1_standart_memory_allocate();
    test2_memory_free_one_block();
    test3_memory_free_two_blocks();
    test4_new_region_from_old();
    test5_new_region_from_new();
    fprintf(stdout, " All tests passed\n");
    return 0;
}


